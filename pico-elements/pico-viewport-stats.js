(() => {
  // src/stats.js
  var browserZoomFactorParams = () => {
    return { globalThis };
  };
  var browserZoomFactor = ({ globalThis: globalThis2 } = browserZoomFactorParams()) => {
    if ("devicePixelRatio" in globalThis2) {
      const devicePixelRatio = globalThis2.devicePixelRatio;
      if (globalThis2.screen && globalThis2.screen.scaleFactor) {
        const scaleFactor = globalThis2.screen.scaleFactor;
        return devicePixelRatio / scaleFactor;
      }
    }
    return null;
  };
  var pinchZoomFactorParams = () => {
    return { globalThis };
  };
  var pinchZoomFactor = ({ globalThis: globalThis2 } = pinchZoomFactorParams()) => {
    if ("visualViewport" in globalThis2 && globalThis2.visualViewport.scale) {
      return globalThis2.visualViewport.scale;
    }
    return null;
  };

  // src/pico-viewport-stats.js
  var debugTemplate = document.createElement("template");
  debugTemplate.innerHTML = `
  <style>
    :host {
    }
  </style>
  <pre></pre>
`;
  var template = document.createElement("template");
  template.innerHTML = `
  <style>
    .main-box {
      margin-bottom: 0;
      border: 1px solid lightgrey;
      border-bottom: 0;
    }
    kbd {
      border-radius: 3px;
      padding: 0.2em;
      white-space: nowrap;
      border: 1px outset lightgrey;
      background: lightgrey;
    }
    
    details > summary {
      list-style: none;
      padding: 0.5em;
      white-space: nowrap;
    }
    details > summary::-webkit-details-marker {
      display: none;
    }
    .secondarySize {
      font-size: 0.7em; 
      color: grey;
    }
    details > summary:hover {
      background: rgba(164, 19, 29, 0.1);
    }
    details > p {
      position: absolute;
      min-width: 10em;
      background: #eee;
      margin: 0.5em;
      text-align: start;
      padding: 1em;
      border-radius: 4px;
      border: 1px outset #eee;
    }
    details > p:before {
      content: "";
      width: 0;
      height: 0;
      border-left: 0.5em solid transparent;
      border-right: 0.5em solid transparent;
      border-bottom: 0.5em solid #eee;
      top: -0.5em;
      position: absolute;
      left: 1em;
    }
    details[open] > p {
      animation: animateShow 0.3s ease-out;
    }    
    @keyframes animateShow {
      0% {
        transform: translatey(2em);
      }
      100% {
        transform: translatey(0);
      }
    }
    
    .zooms summary {
      display: flex;
    }
    .zooms summary span {
      flex: 1;
      margin: 0;
    }
    .zooms summary span:first-child {
      text-align: end;
    }
    .zooms summary span:nth-child(2) {
      padding-left: 1em;
    }
    .zooms .browserZoom,
    .zooms .pinchZoom {
      display: none;
    }
    
  </style>
  
  <div style="text-align: center; border-radius: 4px; border: solid 1px lightgrey;">
    <details>
      <summary>
        <div>\u2190 &nbsp; <slot name="availablePixels.widthPx">???</slot> &nbsp;  \u2192</div>
        <div class="secondarySize" slot="availablePixels.widthRem"></div>
      </summary>
      <p>
        The <b>absolute usable width</b>, available for this site to display things on.<br/>
        As the browser reports it via <code>window.innerWidth</code>.<br/>
      </p>
    </details>
    <div style="flex: 1; display: flex; flex-direction: row; ">
      <div style="flex: 1">
        <br/>
        <details>
          <summary>
            <div slot="main.spaceLeftPercent">???</div>
            <div class="secondarySize" slot="main.spaceLeftPx">???</div>
            <div class="secondarySize" slot="main.spaceLeftRem">???</div>
          </summary>
          <p>
            The <b>space available left</b> of the <code>&lt;main&gt;</code> element.<br/>
            A lot of pages leave this space unused when viewed on big monitors.<br/>
            The percentage is relative to the absolute usable width.
          </p>
        </details>
      </div>
      
      <div class="main-box" style="flex: 1">
        <code>&lt;main&gt;</code><br/>
        <details>
          <summary>
            <slot name="main.widthPercent">???</slot>
            <div class="secondarySize" slot="main.widthPx">???</div>
            <div class="secondarySize" slot="main.widthRem">???</div>
          </summary>
          <p>
            The <b>main content space</b> used up by the <code>&lt;main&gt;</code> element,<br/>
            normally containing the most relevant content of a page like this.<br/>
            The percentage is relative to the absolute usable width.
          </p>
        </details>      
      </div>
      
      <div style="flex: 1">
        <br/>
        <details>
          <summary>
            <slot name="main.spaceRightPercent">???</slot>
            <div class="secondarySize" slot="main.spaceRightPx">???</div>
            <div class="secondarySize" slot="main.spaceRightRem">???</div>
          </summary>
          <p>
            The <b>space available right</b> of <code>&lt;main&gt;</code> element.<br/>
            A lot of pages leave this space unused, when viewed on big monitors.<br/>
            The percentage is relative to the absolute usable width.
          </p>
        </details>      
      </div>
    </div>
  </div>
  <div class="zooms">
    <details class="pinchZoom">
      <summary>
        <span>\u{1F50E} \u{1F90F}</span> 
        <span slot="pinchZoomFactor">???</span>
      </summary>
      <p>
        The <b>pinch zoom</b> in %, or the visual viewport scale.<br/>
        This is the zoom factor effected when pinch-zooming<br/>
        e.g. with two fingers on the screen or the trackpad.<br/>
        It is the value reported by <code>globalThis.visualViewport.scale</code>.
      </p>
    </details>
    <details class="browserZoom">
      <summary>
        <span>\u{1F50E} <kbd>CTRL</kbd> + <kbd>+</kbd></span>
        <span slot="browserZoomFactor">???</span>
      </summary>
      <p>
        The browser, text or <b>content zoom</b> in %.<br/>
        This zoom factor changes e.g. when zooming via the key combo shown above.<br/>
        Mostly this just effects the text size, but this heavily depends on how<br/>
        a website is built.
      </p>
    </details>
  </div>
`;
  var PicoViewportStats = class extends HTMLElement {
    constructor() {
      super();
      this.attachShadow({ mode: "open" });
    }
    _selectInShadowRoot(selector) {
      return this.shadowRoot && this.shadowRoot.querySelector(selector);
    }
    get $debug() {
      return this._selectInShadowRoot("pre");
    }
    get $detailSummariesAll() {
      return this.shadowRoot.querySelectorAll("details > summary");
    }
    $slotByName(name) {
      return this.shadowRoot.querySelector(`slot[name="${name}"], [slot="${name}"]`);
    }
    static get observedAttributes() {
      return ["debug"];
    }
    attributeChangedCallback(name, oldValue, newValue) {
    }
    get _isDebugView() {
      return this.hasAttribute("debug");
    }
    _connectToAutoOpenDetails() {
      const show = (node) => {
        node.setAttribute("open", "open");
      };
      const hide = (node) => {
        node.removeAttribute("open");
      };
      const fn = (summaryNode) => {
        const detailsNode = summaryNode.parentNode;
        detailsNode.addEventListener("mouseover", () => show(detailsNode));
        detailsNode.addEventListener("mouseout", () => hide(detailsNode));
      };
      this.$detailSummariesAll.forEach(fn);
    }
    _disconnectToAutoOpenDetails() {
    }
    connectedCallback() {
      if (this._isDebugView) {
        this.shadowRoot.appendChild(debugTemplate.content.cloneNode(true));
      } else {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this._connectToAutoOpenDetails();
      }
      const onWindowResize = (onResize) => {
        const listenOn = globalThis.visualViewport ? globalThis.visualViewport : globalThis;
        listenOn.addEventListener("resize", onResize);
      };
      onWindowResize(() => {
        this._render();
      });
      globalThis.addEventListener("load", () => {
        this._render();
      });
    }
    disconnectedCallback() {
      if (this._isDebugView) {
      } else {
        this._disconnectToAutoOpenDetails();
      }
    }
    _renderDebugView() {
      this.$debug.innerHTML = this._debugString;
    }
    _render() {
      if (this._isDebugView) {
        this._renderDebugView();
        return;
      }
      const readableInPercent = (v) => Number(v * 100).toFixed(1) + "%";
      const readableInPixels = (v) => Number(v).toFixed(0) + " px";
      const readableInRem = (v) => Number(v).toFixed(1) + " rem";
      const summary = this._summary;
      if (summary.browserZoomFactor) {
        this.$slotByName("browserZoomFactor").innerHTML = readableInPercent(summary.browserZoomFactor);
        this.shadowRoot.querySelector(".zooms .browserZoom").style.display = "block";
      }
      if (summary.pinchZoomFactor) {
        this.$slotByName("pinchZoomFactor").innerHTML = readableInPercent(summary.pinchZoomFactor);
        this.shadowRoot.querySelector(".zooms .pinchZoom").style.display = "block";
      }
      this.$slotByName("availablePixels.widthPx").innerHTML = readableInPixels(summary.availablePixels.widthPx);
      this.$slotByName("availablePixels.widthRem").innerHTML = readableInRem(summary.availablePixels.widthRem);
      this.$slotByName("main.spaceLeftPx").innerHTML = readableInPixels(summary.main.spaceLeftPx);
      this.$slotByName("main.spaceLeftPercent").innerHTML = readableInPercent(summary.main.spaceLeftPercent);
      this.$slotByName("main.spaceLeftRem").innerHTML = readableInRem(summary.main.spaceLeftRem);
      this.$slotByName("main.spaceRightPx").innerHTML = readableInPixels(summary.main.spaceRightPx);
      this.$slotByName("main.spaceRightPercent").innerHTML = readableInPercent(summary.main.spaceRightPercent);
      this.$slotByName("main.spaceRightRem").innerHTML = readableInRem(summary.main.spaceRightRem);
      this.$slotByName("main.widthPx").innerHTML = readableInPixels(summary.main.widthPx);
      this.$slotByName("main.widthPercent").innerHTML = readableInPercent(summary.main.widthPercent);
      this.$slotByName("main.widthRem").innerHTML = readableInRem(summary.main.widthRem);
    }
    get _summary() {
      const availableWidthPx = globalThis.innerWidth;
      const baseFontSizePx = Number.parseInt(globalThis.getComputedStyle(document.body).fontSize);
      const mainStats = () => {
        const $main = document.querySelector("main");
        if ($main) {
          const spaceLeftPx = $main.offsetLeft;
          const widthPx = $main.offsetWidth;
          const spaceRightPx = availableWidthPx - spaceLeftPx - widthPx;
          return {
            widthPx,
            widthPercent: widthPx / availableWidthPx,
            widthRem: widthPx / baseFontSizePx,
            spaceLeftPx,
            spaceLeftPercent: spaceLeftPx / availableWidthPx,
            spaceLeftRem: spaceLeftPx / baseFontSizePx,
            spaceRightPx,
            spaceRightPercent: spaceRightPx / availableWidthPx,
            spaceRightRem: spaceRightPx / baseFontSizePx
          };
        }
        return {};
      };
      return {
        pinchZoomFactor: pinchZoomFactor(),
        browserZoomFactor: browserZoomFactor(),
        availablePixels: {
          widthPx: availableWidthPx,
          widthRem: availableWidthPx / baseFontSizePx
        },
        main: mainStats()
      };
    }
    get _debugString() {
      const s = globalThis.screen;
      const screen = {
        availHeight: s.availHeight,
        availWidth: s.availWidth,
        availTop: s.availTop,
        availLeft: s.availLeft,
        height: s.height,
        width: s.width,
        top: s.top,
        left: s.left,
        scaleFactor: s.scaleFactor,
        pixelDepth: s.pixelDepth
      };
      const window = {
        innerWidth: globalThis.innerWidth,
        innerHeight: globalThis.innerHeight,
        outerWidth: globalThis.outerWidth,
        outerHeight: globalThis.outerHeight,
        devicePixelRatio: globalThis.devicePixelRatio
      };
      const v = globalThis.visualViewport || {};
      const visualViewport = {
        height: v.height,
        offsetLeft: v.offsetLeft,
        offsetTop: v.offsetTop,
        pageLeft: v.pageLeft,
        pageTop: v.pageTop,
        scale: v.scale,
        width: v.width
      };
      const userAgent = {
        baseFontSize: globalThis.getComputedStyle(document.body).fontSize
      };
      const info = { summary: this._summary, parts: { screen, window, visualViewport, userAgent } };
      return JSON.stringify(info, null, 4);
    }
  };
  var defineCustomElement = () => {
    customElements.define("pico-viewport-stats", PicoViewportStats);
  };
  defineCustomElement();
})();
